package com.credibanco.assessment.library.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "author")
public class Author {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Exclude
	private long id;
    
    @Column(name = "name", nullable = false)
	private String name;
	@Column(name = "lastName", nullable = false)
	private String lastName;
	@Column(name = "birthDate", nullable = true)
	private Date birthDate;
	@Column(name = "city", nullable = true)
	private String city;
	@Column(name = "email", nullable = true)
	private String email;

    @OneToMany(mappedBy = "author", fetch = FetchType.LAZY)
	@Builder.Default
	private Set<Book> books = new HashSet();
}
