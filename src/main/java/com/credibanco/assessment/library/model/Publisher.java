package com.credibanco.assessment.library.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "publisher")
public class Publisher {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Exclude
	private long id;
    
	@Column(name = "name", nullable = false)
	private String name;
	@Column(name = "address", nullable = false)
	private String address;
	@Column(name = "phone", nullable = false)
	private String phone;
	@Column(name = "email", nullable = true)
	private String email;
	@Column(name = "maxBooksRegister", nullable = false)
	private @Getter int maxBooksRegister;

	@OneToMany(mappedBy = "publisher", fetch = FetchType.LAZY)
	@Builder.Default
	private Set<Book> books = new HashSet();
}
