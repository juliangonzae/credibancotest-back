package com.credibanco.assessment.library.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "book")
public class Book {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Exclude
	private long id;
    @Column(name = "title", nullable = false)
	public String title;
	@Column(name = "year", nullable = true)
	private int year;
	@Column(name = "genre", nullable = false)
	private String genre;
	@Column(name = "pagesNumber", nullable = false)
	private int pagesNumber;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "publisherId", nullable = true)
	private Publisher publisher;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "authorId", nullable = false)
	private Author author;
}
