package com.credibanco.assessment.library.controller;

import com.credibanco.assessment.library.dto.AuthorDTO;
import com.credibanco.assessment.library.model.Author;
import com.credibanco.assessment.library.service.interfaces.IAuthorService;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/author")
public class AuthorController {
    
    ModelMapper mapper = new ModelMapper();
    
    @Autowired
    IAuthorService _authorService;
    
    @PostMapping
    public ResponseEntity<AuthorDTO> saveAuthor(@Validated @RequestBody AuthorDTO author)
    {
        Author authordb = _authorService.save(author);
        author = this.mapper.map(authordb, AuthorDTO.class);
        
        return ResponseEntity.ok().body(author);
    }
}
