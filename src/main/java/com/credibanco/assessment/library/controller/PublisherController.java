package com.credibanco.assessment.library.controller;

import com.credibanco.assessment.library.dto.PublisherDTO;
import com.credibanco.assessment.library.model.Publisher;
import com.credibanco.assessment.library.service.interfaces.IPublisherService;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/publisher")
public class PublisherController {
    
    ModelMapper mapper = new ModelMapper();
    
    @Autowired
    private IPublisherService _publisherService;

    @PostMapping
    public ResponseEntity<PublisherDTO> savePublisher( @RequestBody PublisherDTO publisher)
    {
        Publisher publisherdb = _publisherService.save(publisher);
        publisher = this.mapper.map(publisherdb, PublisherDTO.class);
        return ResponseEntity.ok().body(publisher);
    }
}
