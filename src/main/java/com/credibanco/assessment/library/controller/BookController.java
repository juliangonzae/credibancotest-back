package com.credibanco.assessment.library.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.credibanco.assessment.library.dto.BookDTO;
import com.credibanco.assessment.library.dto.ResponseErrorDTO;
import com.credibanco.assessment.library.model.Book;
import com.credibanco.assessment.library.service.interfaces.IBookService;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/book")
public class BookController {
    
    ModelMapper mapper = new ModelMapper();
    
    @Autowired
    private IBookService _bookService;

    @PostMapping
    public ResponseEntity saveBook(@Validated @RequestBody BookDTO book)
    {
        try
        {
            Book bookdb = _bookService.save(book, book.getPublisherId(), book.getAuthorId());
            book = this.mapper.map(bookdb, BookDTO.class);
            book.setAuthor(bookdb.getAuthor().getName() + " " + bookdb.getAuthor().getLastName());
            book.setPublisher(bookdb.getPublisher().getName());
            return ResponseEntity.ok().body(book);
        }
        catch(Exception e)
        {
            ResponseErrorDTO err  = ResponseErrorDTO.builder().message(e.getMessage()).build();
            return ResponseEntity.badRequest().body(err);
        }
    }

    @CrossOrigin
    @GetMapping("/search")
    public List<BookDTO> findBooksByFilters(@RequestParam("search") Optional<String> search)
    {
        if(!search.isPresent())
            return _bookService.getAllBooks().stream().map(book -> this.mapper.map(book, BookDTO.class)).collect(Collectors.toList());
        else
            return _bookService.getAllBooksByFilter(search.get()).stream().map(book -> this.mapper.map(book, BookDTO.class)).collect(Collectors.toList());
    }
}
