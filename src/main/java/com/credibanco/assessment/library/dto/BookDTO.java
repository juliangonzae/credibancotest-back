package com.credibanco.assessment.library.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BookDTO {
    private long id;
	private String title;
	private int year;
	private String genre;
	private int pagesNumber;
    private long publisherId;
	private String publisher;
    private long authorId;
	private String author;
}
