package com.credibanco.assessment.library.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PublisherDTO {
    private long id;
	private String name;
	private String address;
	private String phone;
	private String email;
	private int maxBooksRegister;
}
