package com.credibanco.assessment.library.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AuthorDTO {
    public long id;
    private String name;
	private String lastname;
    private Date birthDate;
	private String city;
	private String email;
}
