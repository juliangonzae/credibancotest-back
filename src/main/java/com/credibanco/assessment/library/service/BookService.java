package com.credibanco.assessment.library.service;

import java.util.List;
import java.util.Optional;

import com.credibanco.assessment.library.dto.BookDTO;
import com.credibanco.assessment.library.model.Author;
import com.credibanco.assessment.library.model.Book;
import com.credibanco.assessment.library.model.Publisher;
import com.credibanco.assessment.library.repository.BookRepository;
import com.credibanco.assessment.library.service.interfaces.IAuthorService;
import com.credibanco.assessment.library.service.interfaces.IBookService;
import com.credibanco.assessment.library.service.interfaces.IPublisherService;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

@Service
public class BookService implements IBookService {

    ModelMapper mapper = new ModelMapper();

    @Autowired
    BookRepository _bookRepository;

    @Autowired
    IPublisherService _publisherService;

    @Autowired
    IAuthorService _authorService;
    
    @Override
    public Book save(BookDTO book, long publisherId, long authorId) {
        Optional<Publisher> publisher =  _publisherService.findById(publisherId);
        if(!publisher.isPresent())
            throw new RuntimeException("La editorial no esta registrada.");
        Optional<Author> author = _authorService.findById(authorId);
        if(!author.isPresent())
            throw new RuntimeException("El autor no esta registrado.");
        int maxbooks = publisher.get().getMaxBooksRegister();

        long bookscount = _bookRepository.findByPublisherId(publisherId).stream().count();;
        // long bookscount =_bookRepository.findAll().stream().count();

        if(bookscount >= maxbooks)
            throw new RuntimeException("No es posible registrar el libro, se alcanzó el máximo permitido.");
        Book bookdb = this.mapper.map(book, Book.class);

        return _bookRepository.saveAndFlush(bookdb);
    }

    @Override
    public List<Book> getAllBooks() {
        Iterable<Book> books = _bookRepository.findAll();
        return Streamable.of(books).toList();
    }

    @Override
    public Optional<Book> findById(long id) {
        return _bookRepository.findById(id);
    }

    @Override
    public List<Book> getAllBooksByFilter(String search) {
        return _bookRepository.findBooksByFilters(search);
    }
    
}
