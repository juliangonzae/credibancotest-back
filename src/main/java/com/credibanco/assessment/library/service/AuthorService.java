package com.credibanco.assessment.library.service;

import java.util.List;
import java.util.Optional;

import com.credibanco.assessment.library.dto.AuthorDTO;
import com.credibanco.assessment.library.model.Author;
import com.credibanco.assessment.library.repository.AuthorRepository;
import com.credibanco.assessment.library.service.interfaces.IAuthorService;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

@Service
public class AuthorService implements IAuthorService {

    ModelMapper mapper = new ModelMapper();

    @Autowired
    AuthorRepository authorRepository;
    
    @Override
    public Author save(AuthorDTO author) {
        Author authordb = this.mapper.map(author, Author.class);
        return authorRepository.saveAndFlush(authordb);
    }

    @Override
    public List<Author> getAllAuthors() {
        Iterable<Author> authors = authorRepository.findAll();
        return Streamable.of(authors).toList();
    }

    @Override
    public Optional<Author> findById(long id) {
        return authorRepository.findById(id);
    }
    
}
