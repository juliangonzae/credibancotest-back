package com.credibanco.assessment.library.service;

import java.util.List;
import java.util.Optional;

import com.credibanco.assessment.library.dto.PublisherDTO;
import com.credibanco.assessment.library.model.Publisher;
import com.credibanco.assessment.library.repository.PublisherRepository;
import com.credibanco.assessment.library.service.interfaces.IPublisherService;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;

@Service
public class PublisherService implements IPublisherService{

    ModelMapper mapper = new ModelMapper();

    @Autowired
    PublisherRepository publisherRepository;
    
    @Override
    public Publisher save(PublisherDTO publisher) {
        Publisher publisherdb = this.mapper.map(publisher, Publisher.class);
		return publisherRepository.saveAndFlush(publisherdb);
    }

    @Override
    public List<Publisher> getAllPublishers() {
        Iterable<Publisher> publishers = publisherRepository.findAll();
        return Streamable.of(publishers).toList();
    }

    @Override
    public Optional<Publisher> findById(long id) {
        Optional<Publisher> publisher = publisherRepository.findById(id);
        return publisher;
    }
    
}
