package com.credibanco.assessment.library.service.interfaces;

import java.util.List;
import java.util.Optional;

import com.credibanco.assessment.library.dto.AuthorDTO;
import com.credibanco.assessment.library.model.Author;

public interface IAuthorService {
    public Author save(AuthorDTO author);
    public List<Author> getAllAuthors();
    public Optional<Author> findById(long id);
}
