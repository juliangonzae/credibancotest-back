package com.credibanco.assessment.library.service.interfaces;

import java.util.List;
import java.util.Optional;

import com.credibanco.assessment.library.dto.BookDTO;
import com.credibanco.assessment.library.model.Book;

public interface IBookService {
    public Book save(BookDTO book, long publisherId, long authorId);
    public List<Book> getAllBooks();
    public List<Book> getAllBooksByFilter(String search);
    public Optional<Book> findById(long id);
}
