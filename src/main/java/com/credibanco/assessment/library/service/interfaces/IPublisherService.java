package com.credibanco.assessment.library.service.interfaces;

import java.util.List;
import java.util.Optional;

import com.credibanco.assessment.library.dto.PublisherDTO;
import com.credibanco.assessment.library.model.Publisher;

public interface IPublisherService {
    public Publisher save(PublisherDTO publisher);
    public List<Publisher> getAllPublishers();
    public Optional<Publisher> findById(long id);
}
