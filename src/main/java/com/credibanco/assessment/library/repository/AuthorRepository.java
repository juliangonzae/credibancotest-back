package com.credibanco.assessment.library.repository;

import com.credibanco.assessment.library.model.Author;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long>{
    
}
