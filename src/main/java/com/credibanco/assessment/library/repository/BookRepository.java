package com.credibanco.assessment.library.repository;

import java.util.List;

import com.credibanco.assessment.library.model.Book;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository  extends JpaRepository<Book, Long> {
    List<Book> findByPublisherId(Long publisherId);

    @Query(value =  "SELECT b.id,b.title,b.year,b.genre,b.pages_number,b.author_id,b.publisher_id  FROM book AS b INNER JOIN author AS a ON b.author_id = a.id WHERE b.title = ?1 OR a.name = ?1 OR a.last_name = ?1",
            nativeQuery = true)
    List<Book> findBooksByFilters(String search);
}
