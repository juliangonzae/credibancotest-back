package com.credibanco.assessment.library.repository;

import com.credibanco.assessment.library.model.Publisher;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PublisherRepository extends JpaRepository<Publisher, Long> {
    
}
